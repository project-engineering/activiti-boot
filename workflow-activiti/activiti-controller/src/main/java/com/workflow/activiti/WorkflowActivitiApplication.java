package com.workflow.activiti;

import lombok.extern.log4j.Log4j2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = {"com.workflow.activiti"},exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
        org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class,
        DataSourceAutoConfiguration.class,
        RedisAutoConfiguration.class
})
@Log4j2
//@MapperScan("com.workflow.activiti.mapper")
public class WorkflowActivitiApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkflowActivitiApplication.class, args);
        log.info("Workflow Activiti Application started successfully.");
    }

}
