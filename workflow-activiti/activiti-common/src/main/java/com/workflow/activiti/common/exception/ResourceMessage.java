package com.workflow.activiti.common.exception;

public interface ResourceMessage {
    String getCode();

    Object[] getParams();
}
