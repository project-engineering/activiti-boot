package com.workflow.activiti.common.code;

public interface ICode {
    String getCode();

    String getMessage();
}