package com.workflow.activiti.common.lang;

public interface IsEmpty {
    boolean isEmpty();
}
